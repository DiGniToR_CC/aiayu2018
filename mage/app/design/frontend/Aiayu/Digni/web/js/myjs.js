require(['jquery', 'jquery/ui','mage/gallery/gallery'],
    function(jQuery,gallery){

        var sticky_function =function() {
            jQuery(".product-info-main").addClass('sticky');
            var header = jQuery(".product-info-main.sticky");
            var main_image_height = jQuery('.product-page .main-image .desktop .inner').height();
            var original_header_height = header.height();
            var header_margin = (main_image_height - original_header_height) / 2;
            if (header_margin <= 10){
                header_margin = 10;
                header.css('margin-top',header_margin);
            }else{
                header_margin = (main_image_height - original_header_height) / 2;
                header.css('margin-top',header_margin);
            }
            var header_height = original_header_height + header_margin;
            var el = jQuery('.product-page');
            var el_offset = el.offset().top;
            var el_bottom = el_offset + jQuery('.page-layout-1column .product-page').height();

            header.css('top',el_offset);

            jQuery(window).scroll(function (event) {
                var scroll = jQuery(window).scrollTop();
                if (scroll <= (el_bottom - header_height)){
                    header.css({"top":el_offset,"position":"fixed","bottom":"auto"});
                    header.removeClass('reached-bottom');
                }else{
                    header.css({"top":"auto","position":"absolute","bottom":"0"});
                    header.addClass('reached-bottom');
                }
            });
        };


    jQuery(document).on('swatch.initialized', function() {
        if (jQuery(window).width() < 700) {

        }else{
            sticky_function();
        }

        var product_page_width = jQuery('.product-page').width();
        jQuery('.product-page .product-info-main.sticky').css({'width':product_page_width * 0.4});


        jQuery(".swatch-attribute.color .swatch-option").click(function() {
            var page_offset_top = jQuery('.product-page').offset().top;
            var gallery_offset_top = jQuery(".gallery").offset().top;
            var scroll = jQuery(window).scrollTop();
            var window_width = jQuery(window).width();

            if (window_width >= 700){
                if (scroll <= gallery_offset_top){
                    jQuery('html,body').animate({
                            scrollTop: gallery_offset_top - page_offset_top},
                        1000);
                }else{
                }
            }
        });
    });

        _$main_img = jQuery('.product-page .main-image');

        var $desktop = _$main_img.find('.desktop');
        var force_proportions = function(){

            $desktop.css('height','');

            if($desktop.width()/$desktop.height() < 1.65){
                $desktop.height($desktop.width()/1.65);
            }
            else if($desktop.width()/$desktop.height() > 1.75){
                $desktop.height($desktop.width()/1.75);
            }
        };

        force_proportions();


        jQuery('#search').on('click', function(){
            jQuery('.block-search').toggleClass('open');
        });

        var oldURL = document.referrer;
        var backlink = jQuery('.back-link');
        backlink.attr("href", oldURL);

        if (jQuery(window).width() < 700) {
            var product_add_form = jQuery('#product_addtocart_form');
            jQuery('#mobile-cart-options').append(product_add_form);
            if(jQuery('.mobile-price').length == false){
                var price = jQuery('.product-page .product-info-main .box-tocart .fieldset .price-box .price-wrapper .price').text();
                var html = "<span class='mobile-price'>" + price + "</span>";
                jQuery('.product-info-price ').append(html);
            }

        }



        jQuery(window).on("resize", function(event){
            force_proportions();
            if (jQuery(window).width() < 700) {
                var product_add_form = jQuery('#product_addtocart_form');
                jQuery('#mobile-cart-options').append(product_add_form);
                jQuery('.product-info-main.sticky').removeClass('sticky');
                jQuery('.product-info-main').addClass('mob');
                if(jQuery('.mobile-price').length == false){
                    var price = jQuery('.product-page .product-info-main .box-tocart .fieldset .price-box .price-wrapper .price').text();
                    var html = "<span class='mobile-price'>" + price + "</span>";
                    jQuery('.product-info-price ').append(html);
                }

            }else{
                var product_add_form = jQuery('#product_addtocart_form');
                jQuery('.product-add-form').append(product_add_form);
                jQuery('.product-info-main').removeClass('mob');
                jQuery('.product-info-price .mobile-price').remove();
                sticky_function();
            }

            var product_page_width = jQuery('.product-page').width();
            jQuery('.product-page .product-info-main.sticky').css({'width':product_page_width * 0.4});
        });

        jQuery(".down-link").click(function() {
            var page_offset_top = jQuery('.product-page').offset().top;
            jQuery('html,body').animate({
                    scrollTop: jQuery(".gallery").offset().top - page_offset_top},
                1000);
        });

});