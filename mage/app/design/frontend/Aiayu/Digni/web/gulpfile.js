var gulp         = require('gulp');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var argv         = require('yargs').argv;
var uglify       = require("gulp-uglifyes");
var concat       = require('gulp-concat-util');
var gif          = require('gulp-if');
var sourcemaps   = require('gulp-sourcemaps');
// var grunt        = require('grunt');

var isProd = argv.prod;

var appScripts = [
    "./src/js/din.js",
    "./src/js/**/*.js"
];

// --------

// grunt.initConfig({
//     svgstore: {
//         options: {
//             prefix: 'shape-', // This will prefix each <g> ID
//         },
//         default: {
//             files: {
//                 './sprites/svg-defs.svg': ['./src/sprites/svg_sprites/*.svg'],
//             }
//         }
//     },
// });

// grunt.loadNpmTasks('grunt-svgstore');
//
// gulp.task('svgstore',function(){
//     grunt.tasks(
//         ['svgstore'],
//         {gruntfile: false}
//     );
// });

// -----------
gulp.task('js',function(){
    return gulp.src(appScripts)
        // Concat
        .pipe(concat('app.js'))
        // If production, then minify
        .pipe(gif(isProd,uglify().on('error',function(data){console.log(data);})))
        // Save
        .pipe(gulp.dest('./js'));

});

gulp.task('sass', function() {
    return gulp.src('./src/scss/**/*.scss')


        // .pipe(gulp.dest('./css'));
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        // .pipe(sass({
        //     outputStyle: 'compressed'
        // }))
        .on('error', onError)
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./css'));

});

gulp.task('watch', function() {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    // gulp.watch('./src/sprites/svg_sprites/*.svg', ['svgstore']);
});
gulp.task('build', ['js', 'sass']);
gulp.task('default', ['js', 'sass', 'watch']);

function onError(err) {
    console.log(err.message);
    this.emit('end');
}